//
//  JSONParser.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

enum JSONDecoderError: LocalizedError {
    case error(error: Error)
    case empty
    
    var errorDescription: String? {
        switch self {
        case .error: return "MyError.testError"
        case .empty: return "MyError.unknownError"
        }
    }
}

struct JSONParser {
    
    static func decode<T: Decodable>(type: T.Type, data: Data?) -> Result<T, Error> {
        if let data = data {
            do {
                let response = try JSONDecoder().decode(T.self, from: data)
                return .success(response)
            } catch let error {
                return .failure(JSONDecoderError.error(error: error))
            }
        }
        return .failure(JSONDecoderError.empty)
    }
}
