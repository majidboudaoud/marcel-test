//
//  Logger.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 18/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

public class Logger {
    public class func warning(_ object: Any, filename: String = #file, line: Int = #line, funcName: String = #function) {
        Logger.log(object, logEvent: .warning, filename: filename, line: line, funcName: funcName)
    }
    
    public class func debug(_ object: Any, filename: String = #file, line: Int = #line, funcName: String = #function) {
        Logger.log(object, logEvent: .debug, filename: filename, line: line, funcName: funcName)
    }
    
    public class func error(_ object: Any, filename: String = #file, line: Int = #line, funcName: String = #function) {
        Logger.log(object, logEvent: .error, filename: filename, line: line, funcName: funcName)
    }
}

private extension Logger {
    
    class func log(_ object: Any, logEvent: LogEvent, filename: String, line: Int, funcName: String) {
        print("\(logEvent.descriptionLog) \(sourceFileName(filePath: filename)):\(line) \(funcName) -> \(object)")
    }
    
    class func sourceFileName(filePath: String) -> String {
        return filePath.components(separatedBy: "/").last ?? ""
    }
    
    enum LogEvent {
        case error
        case warning
        case debug
        
        var descriptionLog: String {
            switch self {
            case .error: return "[ERROR 🚫]"
            case .warning: return "[WARNING ⚠️]"
            case .debug: return "[DEBUG]"
            }
        }
    }
}

