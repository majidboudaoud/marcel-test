//
//  LocationType.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

enum LocationType: String, CaseIterable, Codable {
    
    case work = "work"
    case home = "home"
    
    var title: String {
        switch self {
        case .work:
            return "Travail"
        case .home:
            return "Maison"
        }
    }
    
    var image: UIImage? {
        switch self {
        case .work:
            return UIImage(named: "Work")
        case .home:
            return UIImage(named: "House")
        }
    }
}
