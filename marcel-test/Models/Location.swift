//
//  Location.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

class Location: Codable {
    let type: LocationType
    let address: String
    let lat: Double
    let long: Double
}
