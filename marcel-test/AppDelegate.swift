//
//  AppDelegate.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 18/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupGoogleMapsSDK()
        setupWindow()
        return true
    }
    
    private func setupGoogleMapsSDK() {
        GMSServices.provideAPIKey(Environnement.googleMapAPIKey)
        GMSPlacesClient.provideAPIKey(Environnement.googleMapAPIKey)
    }

    private func setupWindow() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let homeViewController = HomeViewController()
        let navigationController = UINavigationController(rootViewController: homeViewController)
        navigationController.navigationBar.isHidden = true
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        self.window = window
    }

}

