//
//  HTTPNetworkManager.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 18/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

import Foundation

enum HTTPStatusCode: Int {
    case unauthorized = 401
    case failure = 400
    case success = 200
}

enum HTTPMethod: String {
    case GET = "GET"
    case POST = "POST"
}

enum HTTPNetworkError: LocalizedError {
    case invalidURL
    case unknownError
    case unauthorized
    case failure
    case session(description: String)
    
    var errorDescription: String? {
        switch self {
        case .invalidURL: return "MyError.testError"
        case .unknownError: return "MyError.unknownError"
        case .unauthorized: return "MyError.unauthorized"
        case .session(let description): return description
        case .failure: return "MyError.failure"
        }
    }
}

fileprivate struct Constants {
    static let timeout: TimeInterval = TimeInterval(10)
}

struct HTTPNetworkManager {
    
    static func sendRequest(urlString: String, parameters: [AnyHashable: Any], method: HTTPMethod, completion: @escaping(Result<Data?, Error>) -> Void) {
        let url = URL(string: urlString)
        switch url {
        case .none:
            completion(.failure(HTTPNetworkError.invalidURL))
        case .some(let url):
            let request = buildRequest(url: url, parameters: parameters, method: method)
            sendRequest(request: request, completion: { completion($0) })
        }
    }
    
    /// Build a request to send to the API
    ///
    /// - Parameters:
    ///   - url: The complete url to the API endpoint
    ///   - parameters: A key/value dictionnary as a parameter to the request
    ///   - method: A HTTP method for sending the request
    /// - Returns: URLRequest object
    private static func buildRequest(url: URL, parameters: [AnyHashable: Any], method: HTTPMethod) -> URLRequest {
        var request = URLRequest(url: url, cachePolicy: .reloadRevalidatingCacheData, timeoutInterval: Constants.timeout)
        request.httpMethod = method.rawValue
        HTTPNetworkManager.addHeaders(request: &request)
        HTTPNetworkManager.addParameters(request: &request, parameters: parameters)
        return request
    }
    
    /// Add configuration headers such as 'content-type' and 'authorization'
    ///
    /// - Parameter request: the request to configure
    private static func addHeaders(request: inout URLRequest) {
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
    }
    
    /// Add the parameters to the request body by serializing a key/value pair object
    ///
    /// - Parameters:
    ///   - request: the request which is gonna be filled
    ///   - parameters: the key/value pair object describing the parameters to send
    private static func addParameters(request: inout URLRequest, parameters: [AnyHashable: Any]) {
        do {
            guard parameters.count > 0 else { return }
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            Logger.error(error)
        }
    }
    
    private static func sendRequest(request: URLRequest, completion: @escaping(Result<Data?, Error>) -> Void) {
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let response = response as? HTTPURLResponse {
                let statusCode = HTTPStatusCode(rawValue: response.statusCode)
                switch statusCode {
                case .none:
                    completion(.failure(HTTPNetworkError.unknownError))
                case .some(.failure):
                    completion(.failure(HTTPNetworkError.failure))
                case .some(.success):
                    completion(.success(data))
                case .some(.unauthorized):
                    completion(.failure(HTTPNetworkError.unauthorized))
                }
            }
            if let error = error {
                Logger.error(error)
                completion(.failure(HTTPNetworkError.session(description: error.localizedDescription)))
            }
        }.resume()
    }
}
