//
//  HomeSearchBarWorker.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

class HomeSearchBarWorker {
    
    func fetchUserFavorites(completion: @escaping(Result<HomeSearchBarModel.Response, Error>) -> Void) {
        let urlString = "\(Environnement.APIEndpoint)/fztyg"
        HTTPNetworkManager.sendRequest(urlString: urlString, parameters: [:], method: .GET) { (result) in
            switch result {
            case .success(let data):
                completion(JSONParser.decode(type: HomeSearchBarModel.Response.self, data: data))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}
