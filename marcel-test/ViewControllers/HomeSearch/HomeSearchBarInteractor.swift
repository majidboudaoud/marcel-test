//
//  HomeSearchViewInteractor.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

class HomeSearchBarInteractor {
    
    private let worker = HomeSearchBarWorker()
    private var presenter: HomeSearchBarPresenter?
    
    var favorites: [Location]?
    
    func configure(presenter: HomeSearchBarPresenter) {
        self.presenter = presenter
    }
    
    func getFavoriteLocationAt(index: Int) -> Location? {
        return favorites?[index]
    }
    
    func fetchUserFavorites() {
        worker.fetchUserFavorites { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.favorites = response.favorites
                self.presenter?.presentUserFavorites(response: response)
            case .failure(let error):
                self.presenter?.presentError(error: error)
            }
        }
    }
}
