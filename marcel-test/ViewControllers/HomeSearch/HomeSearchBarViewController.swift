//
//  SearchViewController.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 18/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit
import GooglePlaces

enum SearchSuggestionSection: Int, CaseIterable {
    case searchBar
    case suggestions
}

protocol HomeSearchDelegate: class {
    func didTapOnSearchBar()
    func didTapOnSuggestion(suggestion: Location)
}

protocol HomeFavoritesPresentationLogic: class {
    func updateFavorites(favorites: HomeSearchBarModel.ViewModel)
}

class HomeSearchBarViewController: UIViewController, HomeFavoritesPresentationLogic, UICollectionViewDelegate {
    
    weak var delegate: HomeSearchDelegate?
    
    private let interactor = HomeSearchBarInteractor()
    private let dataSource = HomeSearchCollectionViewDataSource()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: view.frame, collectionViewLayout: LinearLayout())
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .white
        HomeSearchCollectionViewDataSource.registerCellsFor(collectionView: collectionView)
        collectionView.dataSource = dataSource
        collectionView.delegate = self
        return collectionView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupDependencies()
        interactor.fetchUserFavorites()
    }
    
    private func setupDependencies() {
        let presenter = HomeSearchBarPresenter()
        presenter.configure(viewController: self)
        interactor.configure(presenter: presenter)
    }
    
    private func setupView() {
        view.addSubview(collectionView)
    }
    
    func updateFavorites(favorites: HomeSearchBarModel.ViewModel) {
        dataSource.configure(viewModel: favorites)
        collectionView.reloadData()
    }
    
    // MARK: UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let section = SearchSuggestionSection(rawValue: indexPath.item)
        switch section {
        case .some(.searchBar):
            delegate?.didTapOnSearchBar()
        case .some(.suggestions):
            guard let favoriteItem = interactor.getFavoriteLocationAt(index: indexPath.item) else { return }
            delegate?.didTapOnSuggestion(suggestion: favoriteItem)
        case .none:
            break
        }
    }
}
