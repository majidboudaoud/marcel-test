//
//  HomeSearchViewPresenter.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation

class HomeSearchBarPresenter {
    
    private weak var viewController: HomeFavoritesPresentationLogic?
    
    func configure(viewController: HomeFavoritesPresentationLogic) {
        self.viewController = viewController
    }
    
    func presentError(error: Error) {
        // TODO - show error
    }
    
    func presentUserFavorites(response: HomeSearchBarModel.Response) {
        let displayedFavorites = response.favorites.map {
            HomeSearchBarModel.ViewModel.DisplayedFavorite(title: $0.type.title,
                                                       address: $0.address,
                                                       image: $0.type.image)
        }
        let viewModel = HomeSearchBarModel.ViewModel(displayedFavorites: displayedFavorites)
        DispatchQueue.main.async { [weak self] in
            self?.viewController?.updateFavorites(favorites: viewModel)
        }
    }
}
