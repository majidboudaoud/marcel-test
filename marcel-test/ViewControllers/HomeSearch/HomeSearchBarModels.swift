//
//  HomeSearchModels.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

enum HomeSearchBarModel {
    
    struct Request {
    }
    
    struct Response: Codable {
        let favorites: [Location]
    }
    
    struct ViewModel {
        struct DisplayedFavorite {
            let title: String?
            let address: String?
            let image: UIImage?
        }
        var displayedFavorites: [DisplayedFavorite]
    }
}
