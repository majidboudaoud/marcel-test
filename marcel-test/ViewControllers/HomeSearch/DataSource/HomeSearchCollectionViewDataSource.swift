//
//  SearchCollectionViewDataSource.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 18/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

class HomeSearchCollectionViewDataSource: NSObject, UICollectionViewDataSource {
    
    private var favorites: [HomeSearchBarModel.ViewModel.DisplayedFavorite] = []
    
    class func registerCellsFor(collectionView: UICollectionView) {
        collectionView.register(cellType: SearchSuggestionCollectionViewCell.self)
        collectionView.register(cellType: SearchBarCollectionViewCell.self)
    }
    
    func configure(viewModel: HomeSearchBarModel.ViewModel) {
        self.favorites = viewModel.displayedFavorites
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return SearchSuggestionSection.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let section = SearchSuggestionSection(rawValue: section)
        switch section {
        case .some(.searchBar):
            return 1
        case .some(.suggestions):
            return favorites.count
        case .none:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let section = SearchSuggestionSection(rawValue: indexPath.section)
        
        switch section {
        case .some(.searchBar):
            let cell = collectionView.dequeueReusableCell(with: SearchBarCollectionViewCell.self, for: indexPath)
            return cell
        case .some(.suggestions):
            let cell = collectionView.dequeueReusableCell(with: SearchSuggestionCollectionViewCell.self, for: indexPath)
            let favorite = favorites[indexPath.item]
            cell.configure(title: favorite.title, subtitle: favorite.address, image: favorite.image)
            return cell
        case .none:
            let cell = collectionView.dequeueReusableCell(with: SearchSuggestionCollectionViewCell.self, for: indexPath)
            return cell
        }
    }
}
