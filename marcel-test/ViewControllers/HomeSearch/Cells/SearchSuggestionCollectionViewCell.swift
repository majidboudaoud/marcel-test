//
//  SearchSuggestionCollectionViewCell.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 18/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit
import SnapKit

class SearchSuggestionCollectionViewCell: UICollectionViewCell {
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        return label
    }()
    
    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textColor = .lightGray
        return label
    }()
    
    private let delimiter: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .lightGray
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(title: String?, subtitle: String?, image: UIImage?) {
        self.titleLabel.text = title
        self.subtitleLabel.text = subtitle
        self.imageView.image = image
    }
    
    private func setupView() {
        contentView.backgroundColor = .white
        
        contentView.addSubviews(imageView, titleLabel, subtitleLabel, delimiter)
        
        imageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(contentView)
            make.width.equalTo(27)
            make.height.equalTo(23)
            make.left.equalTo(contentView).offset(16)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(imageView.snp.right).offset(16)
            make.right.equalTo(contentView).inset(16)
            make.top.equalTo(contentView).offset(9)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(imageView.snp.right).offset(16)
            make.right.equalTo(contentView).inset(16)
            make.top.equalTo(titleLabel.snp.bottom).offset(3)
        }
        
        delimiter.snp.makeConstraints { (make) in
            make.bottom.right.equalTo(contentView)
            make.height.equalTo(0.5)
            make.left.equalTo(self).offset(58)
        }
    }
}
