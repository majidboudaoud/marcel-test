//
//  SearchPresenter.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import GooglePlaces

class SearchPresenter {
    
    private weak var viewController: SearchLocationDisplayLogic?
    
    func configure(viewController: SearchLocationDisplayLogic) {
        self.viewController = viewController
    }
    
    func presentError(error: Error) {
        // TODO - present error
    }
    
    func presentSearchResults(response: SearchLocations.Response) {
        let results = response.searchResults.map { SearchLocations.ViewModel.DisplayedResult(title: $0.attributedPrimaryText.string,
                                                                                             subtitle: $0.attributedPrimaryText.string,
                                                                                             image: UIImage(named: "Marker")) }
        
        let viewModel = SearchLocations.ViewModel(displayedResults: results)
        DispatchQueue.main.async { [weak self] in
            self?.viewController?.updateSearchResults(viewModel: viewModel)
        }
    }
}
