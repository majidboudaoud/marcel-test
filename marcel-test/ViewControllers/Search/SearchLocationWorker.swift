//
//  SearchLocationWorker.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import GooglePlaces

class SearchLocationWorker {
    
    private let client = GMSPlacesClient.shared()
    private let token = GMSAutocompleteSessionToken()
    
    func fetchSearchResultsFromQuery(query: String, completion: @escaping(Result<[GMSAutocompletePrediction], Error>) -> Void) {
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        client.findAutocompletePredictions(fromQuery: query,
                                           bounds: nil,
                                           boundsMode: GMSAutocompleteBoundsMode.bias,
                                           filter: filter,
                                           sessionToken: token,
                                           callback: { (results, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            if let results = results {
                completion(.success(results))
                return
            }
        })
    }
}
