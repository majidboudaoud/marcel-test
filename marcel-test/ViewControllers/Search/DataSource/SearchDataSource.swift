//
//  SearchDataSource.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

class SearchDataSource: NSObject, UICollectionViewDataSource {

    private var viewModels: [SearchLocations.ViewModel.DisplayedResult] = []
    
    class func registerCellsFor(collectionView: UICollectionView) {
        collectionView.register(cellType: SearchSuggestionCollectionViewCell.self)
    }
    
    func configure(viewModels: [SearchLocations.ViewModel.DisplayedResult]) {
        self.viewModels = viewModels
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: SearchSuggestionCollectionViewCell.self, for: indexPath)
        let viewModel = viewModels[indexPath.item]
        cell.configure(title: viewModel.title, subtitle: viewModel.subtitle, image: viewModel.image)
        return cell
    }
}
