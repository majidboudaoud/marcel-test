//
//  SearchLocationModels.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import GooglePlaces
import UIKit

enum SearchLocations {
    
    struct Request {
        var query: String
    }
    
    struct Response {
        var searchResults: [GMSAutocompletePrediction]
    }
    
    struct ViewModel {
        struct DisplayedResult {
            let title: String?
            let subtitle: String?
            let image: UIImage?
        }
        var displayedResults: [DisplayedResult]
    }
}
