//
//  SearchViewController.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 18/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit
import GooglePlaces
import SnapKit

protocol SearchLocationDisplayLogic: class {
    func updateSearchResults(viewModel: SearchLocations.ViewModel)
}

class SearchViewController: UIViewController, SearchLocationDisplayLogic {
    
    private let interactor = SearchLocationInteractor()
    private let dataSource = SearchDataSource()
    
    private lazy var originSearchBar: SelectedLocationView  = {
        let searchBar = SelectedLocationView()
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.backgroundColor = .white
        return searchBar
    }()

    private lazy var destinationSearchBar: SearchBar = {
        let searchBar = SearchBar()
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.updateDelegate = self
        searchBar.setLeftImage(image: UIImage(named: "Marker"))
        searchBar.backgroundColor = .white
        searchBar.leftViewMode = .always
        searchBar.placeholder = "Où allez-vous ?"
        return searchBar
    }()

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: LinearLayout())
        SearchDataSource.registerCellsFor(collectionView: collectionView)
        collectionView.dataSource = self.dataSource
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        return collectionView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupDependencies()
    }
    
    private func setupDependencies() {
        let presenter = SearchPresenter()
        presenter.configure(viewController: self)
        interactor.configure(presenter: presenter)
    }

    func updateSearchResults(viewModel: SearchLocations.ViewModel) {
        dataSource.configure(viewModels: viewModel.displayedResults)
        collectionView.reloadData()
    }

    private func setupView() {
        view.addSubviews(originSearchBar, destinationSearchBar, collectionView)
        
        originSearchBar.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(view)
            make.height.equalTo(55)
        }
        destinationSearchBar.snp.makeConstraints { (make) in
            make.top.equalTo(originSearchBar.snp.bottom)
            make.left.right.equalTo(view)
            make.height.equalTo(55)
        }
        collectionView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(view)
            make.top.equalTo(destinationSearchBar.snp.bottom)
        }
    }
}

extension SearchViewController: SearchBarDelegate {
    
    func textDidChange(text: String) {
        let request = SearchLocations.Request(query: text)
        interactor.fetchSearchResultFrom(request: request)
    }
}

