//
//  SearchInteractor.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import GooglePlaces

class SearchLocationInteractor {
    
    private let worker = SearchLocationWorker()
    private var presenter: SearchPresenter?
    
    func configure(presenter: SearchPresenter) {
        self.presenter = presenter
    }
    
    func fetchSearchResultFrom(request: SearchLocations.Request) {
        worker.fetchSearchResultsFromQuery(query: request.query) { [weak self] (result) in
            switch result {
            case .failure(let error):
                self?.presenter?.presentError(error: error)
            case .success(let searchResults):
                let response = SearchLocations.Response(searchResults: searchResults)
                self?.presenter?.presentSearchResults(response: response)
            }
        }
    }
}
