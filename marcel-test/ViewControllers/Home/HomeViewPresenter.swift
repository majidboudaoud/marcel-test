//
//  HomeViewPresenter.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import GoogleMaps

private struct Constants {
    static let zoomLevel: Float = 15.0
}

class HomeViewPresenter: NSObject {
    
    private weak var viewController: LocationUpdateDisplayLogic?
    
    func configure(viewController: LocationUpdateDisplayLogic) {
        self.viewController = viewController
    }
    
    func presentLocationUpdate(location: CLLocation) {
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: Constants.zoomLevel)
        viewController?.updateCamera(camera: camera)
    }
}
