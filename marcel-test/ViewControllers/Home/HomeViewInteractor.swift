//
//  HomeViewInteractor.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import GoogleMaps

protocol LocationUpdateDelegate: class {
    func didUpdateLocation(location: CLLocation)
}

class HomeViewInteractor: NSObject, LocationUpdateDelegate {
    
    private let worker = LocationService()
    private var presenter: HomeViewPresenter?
    
    func configure(presenter: HomeViewPresenter) {
        self.presenter = presenter
    }
    
    func registerToLocationUpdates() {
        worker.registerToLocationUpdates(delegate: self)
    }
    
    func didUpdateLocation(location: CLLocation) {
        presenter?.presentLocationUpdate(location: location)
    }
}
