//
//  HomeViewRouter.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import UIKit

class HomeViewRouter: HomeSearchDelegate {
    
    private weak var navigationController: UINavigationController?
    
    func configure(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }
    
    func didTapOnSearchBar() {
        let searchViewController = SearchViewController()
        navigationController?.present(searchViewController, animated: true, completion: nil)
    }
    
    func didTapOnSuggestion(suggestion: Location) {
        let searchViewController = DriveProposalViewController()
        navigationController?.present(searchViewController, animated: true, completion: nil)
    }
}
