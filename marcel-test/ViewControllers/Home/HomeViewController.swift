//
//  ViewController.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 18/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit
import GoogleMaps
import SnapKit

protocol LocationUpdateDisplayLogic: class {
    func updateCamera(camera: GMSCameraPosition)
}

class HomeViewController: UIViewController, LocationUpdateDisplayLogic {
    
    private let router: HomeViewRouter = HomeViewRouter()
    private let interactor: HomeViewInteractor = HomeViewInteractor()
    
    private lazy var searchViewController: HomeSearchBarViewController = {
        let searchViewController = HomeSearchBarViewController()
        searchViewController.view.translatesAutoresizingMaskIntoConstraints = false
        searchViewController.view.layer.cornerRadius = 8
        searchViewController.view.layer.masksToBounds = true
        return searchViewController
    }()
    
    private lazy var mapView: GMSMapView = {
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: view.frame, camera: camera)
        mapView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        return mapView
    }()
    
    private let bannerView: BannerView = {
        let bannerView = BannerView()
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        return bannerView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupDependencies()
        interactor.registerToLocationUpdates()
    }

    private func setupDependencies() {
        let presenter: HomeViewPresenter = HomeViewPresenter()
        presenter.configure(viewController: self)
        interactor.configure(presenter: presenter)
        router.configure(navigationController: navigationController)
        searchViewController.delegate = router
    }
    
    func updateCamera(camera: GMSCameraPosition) {
        mapView.animate(to: camera)
    }
    
    private func setupView() {
        view.addSubviews(mapView, bannerView, searchViewController.view)
        
        bannerView.snp.makeConstraints { (make) in
            make.height.equalTo(112)
            make.left.right.bottom.equalTo(view)
        }
        searchViewController.view.snp.makeConstraints { (make) in
            make.centerY.equalTo(view)
            make.left.right.equalTo(view).inset(20)
            make.height.equalTo(180)
        }
    }
}


