//
//  UICollectionView+Extensions.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 18/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import UIKit

public extension NSObject {
    class var className: String {
        return String(describing: self.classForCoder())
    }
}

public extension UICollectionView {
    
    func dequeueReusableCell<T: UICollectionViewCell>(with type: T.Type, for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier: type.className, for: indexPath) as! T
    }
    
    func dequeueReusableSupplementaryView<T: UICollectionReusableView>(with type: T.Type, kind: String, for indexPath: IndexPath) -> T {
        return self.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: type.className, for: indexPath) as! T
    }
    
    func registerHeaderReusableView<T: UICollectionReusableView>(cellType: T.Type) {
        register(cellType,
                 forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                 withReuseIdentifier: cellType.className)
    }
    
    func register<T: UICollectionViewCell>(cellType: T.Type) {
        register(cellType, forCellWithReuseIdentifier: cellType.className)
    }
    
    func register<T: UICollectionViewCell>(cells: [T.Type]) {
        cells.forEach{ register(cellType: $0) }
    }
}
