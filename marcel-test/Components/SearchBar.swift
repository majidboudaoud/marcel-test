//
//  SearchBar.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import UIKit

protocol SearchBarDelegate: class {
    func textDidChange(text: String)
}

private struct Constants {
    static let leftViewHeight: CGFloat = 23
    static let textRectOffset: CGFloat = 22
}

class SearchBar: UITextField {
    
    weak var updateDelegate: SearchBarDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func textFieldDidChange() {
        guard let text = self.text else { return }
        updateDelegate?.textDidChange(text: text)
    }
    
    public func setLeftImage(image: UIImage?) {
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        self.leftView = imageView
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.textRect(forBounds: bounds)
        return CGRect(x: rect.minX + Constants.textRectOffset,
                      y: rect.minY,
                      width: rect.width,
                      height: rect.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return self.textRect(forBounds: bounds)
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 18, y: bounds.midY - (Constants.leftViewHeight / 2), width: Constants.leftViewHeight, height: Constants.leftViewHeight)
    }
    
}
