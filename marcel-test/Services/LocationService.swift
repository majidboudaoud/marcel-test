//
//  LocationService.swift
//  marcel-test
//
//  Created by Majid Boudaoud on 19/10/2019.
//  Copyright © 2019 Majid Boudaoud. All rights reserved.
//

import Foundation
import CoreLocation

private struct Constants {
    static let distanceFilter: Double = 50.0
}

class LocationService: NSObject, CLLocationManagerDelegate {
    
    weak var delegate: LocationUpdateDelegate?
    
    private let locationManager: CLLocationManager = {
        let locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = Constants.distanceFilter
        return locationManager
    }()
    
    func registerToLocationUpdates(delegate: LocationUpdateDelegate) {
        self.delegate = delegate
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocation = locations.last else { return }
        delegate?.didUpdateLocation(location: location)
    }
}
